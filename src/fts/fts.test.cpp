#include "../../src/fts/fts.hpp"
#include "../../external/googletest/googletest/include/gtest/gtest.h"

TEST(FirstTest, basicSum) {
  float expect = 2.28;           // wait
  float assert = sum(1.18, 1.1); // real

  ASSERT_EQ(assert, expect);
}
