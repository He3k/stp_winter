#include "../external/cxxopts/include/cxxopts.hpp"
#include "fts/fts.hpp"
#include <iostream>

using namespace std;

int main(int argc, char **argv) {
  cxxopts::Options options("summator", "One line description of summator");
  options.add_options()("f,first", "First digital", cxxopts::value<float>())(
      "s,second", "Second digital", cxxopts::value<float>());
  auto result = options.parse(argc, argv);
  if (result.count("f") == 1 || result.count("s") == 1) {
    auto first = result["first"].as<float>();
    auto second = result["second"].as<float>();
    cout << sum(first, second) << endl;
  } else {
    cout << options.help() << endl;
  }
  return 0;
}